# TODO: Write documentation for `Xtokio`
require "kemal"

module Xtokio
  VERSION = "0.1.0"

  # TODO: Put your code here
  get "/xtokio" do |env|
    "Hola Mundo web!"
  end

  get "/xtokio/hola/:nombre" do |env|
    nombre = env.params.url["nombre"]
    "Hola #{nombre}, como estas?"
  end

end
Kemal.run(3007)